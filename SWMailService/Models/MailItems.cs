﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWMailService.Models {
    public class MailItems {
        [JsonIgnore]
        public Guid mail_id { get; set; }
        [ForeignKey("mail_id")]
        public Mail mail { get; set; }

        [JsonIgnore]
        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }

        public int count { get; set; }
    }
}
