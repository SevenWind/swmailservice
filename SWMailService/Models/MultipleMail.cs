﻿using System;
using System.Collections.Generic;

namespace SWMailService.Models {
    public class MultipleMail {
        public Guid fromId { get; set; }
        public string fromName { get; set; }
        public Guid toId { get; set; }
        public string toName { get; set; }
        public string theme { get; set; }
        public string message { get; set; }

        public List<ExternalMailItem> items { get; set; }
        public string[] userIds { get; set; }
    }
}
