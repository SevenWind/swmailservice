﻿namespace SWMailService.Models {
    public class Enums {
        public enum ItemCode {
            Coins = 1,
            Gold,
            ScrollOfHiring,
            MagicAmulet,
            GekkelKilnFragment,
            DornKirpichFragment,
            CerberFragment,
            TorilBuiniyFragment,
            LiaAlleriaFragment,
            HemingRouterFragment,
        }

        public enum MailBoxType {
            Incoming = 0,
            Outgoing = 1
        }

        public static class AuthRole {
            public const string ADMIN = "Admin";
            public const string PLAYER = "Player";
            public const string TESTER = "Tester";
            public const string ANY = "Admin, Player, Tester";
        }
    }
}
