﻿using System;

namespace SWMailService.Models {
    public class ExternalMailItem {
        public Guid item_id { get; set; }
        public int count { get; set; }
    }
}
