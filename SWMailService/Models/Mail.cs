﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SWMailService.Models {
    public class Mail {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid id { get; set; }
        public Guid fromId { get; set; }
        public string fromName { get; set; }
        public Guid toId { get; set; }
        public string toName { get; set; }
        public string theme { get; set; }
        public string date { get; set; }
        public string message { get; set; }
        public bool isReaded { get; set; }
        public bool attachmentCollected { get; set; }
        public List<MailItems> items { get; set; }
        public bool isDeleted { get; set; }
    }
}
