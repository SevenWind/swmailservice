﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWMailService.Models;
using static SWMailService.Models.Enums;

namespace SWMailService.Controllers
{
    [Route ("api/v1/[controller]/[action]")]
    [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class AdminController : ControllerBase {

        private readonly SWMailDbContext db;
        private readonly ILogger<AdminController> logger;

        public AdminController (SWMailDbContext _db, ILogger<AdminController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        /// <summary>
        /// Удаление предмета(установка флага isDeleted в true)
        /// </summary>
        /// <param name="itemId">ИД предмета</param>
        [HttpPost ("{itemId}")]
        public async Task<IActionResult> DeleteItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = db.Items.Find (itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isDeleted = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Восстановление предмета
        /// </summary>
        /// <param name="itemId">ИД предмета</param>
        [HttpPost ("{itemId}")]
        public async Task<IActionResult> RestoreItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = db.Items.Find (itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isDeleted = false;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Сохранение предмета
        /// </summary>
        /// <param name="item">Модель с данными предмета</param>
        [HttpPost]
        public async Task<IActionResult> SaveItem ([FromBody] Item item) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbItem = await db.Items.FirstOrDefaultAsync (a => a.id == item.id);
                    if (dbItem == null) {
                        // create new
                        dbItem = new Item ();
                        dbItem.id = item.id;
                        dbItem.name = item.name;
                        dbItem.code = item.code;
                        dbItem.image = item.image;
                        dbItem.rarity_id = item.rarity_id;
                        dbItem.characterId = item.characterId;
                        await db.Items.AddAsync (dbItem);
                        await db.SaveChangesAsync ();
                        tr.Commit ();
                        return Ok ();
                    }
                    // update
                    dbItem.name = item.name;
                    dbItem.code = item.code;
                    if (!string.IsNullOrWhiteSpace (item.image) && item.image != dbItem.image)
                        dbItem.image = item.image;
                    dbItem.rarity_id = item.rarity_id;
                    dbItem.characterId = item.characterId;

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}