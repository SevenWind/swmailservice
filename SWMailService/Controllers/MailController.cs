﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWMailService.Models;
using SWMailService.Utility;
using static SWMailService.Models.Enums;

namespace SWMailService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [ApiController]
    public class MailController : ControllerBase {

        private readonly SWMailDbContext db;
        private readonly ILogger<MailController> logger;

        public MailController (SWMailDbContext _db, ILogger<MailController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        /// <summary>
        /// Получение списка сообщений с фильтром и пагинацией
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="search">Строка поиска (от кого\кому или тема)</param>
        /// <param name="type">тип (входящие\исходящие)</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">размер страницы</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet ("userId={userId}&search={search}&type={type}&pageNumber={pageNumber}&pageSize={pageSize}")]
        public IActionResult GetMessages (Guid userId, string search, MailBoxType type, int pageNumber, int pageSize) {
            try {
                Mail[] fullMails;
                IQueryable<Mail> iQueryableMails;
                switch (type) {
                    case MailBoxType.Incoming:
                        iQueryableMails = db.Mails.Include (a => a.items).Where (a => a.toId == userId && !a.isDeleted);
                        break;
                    case MailBoxType.Outgoing:
                        iQueryableMails = db.Mails.Include (a => a.items).Where (a => a.fromId == userId && !a.isDeleted);
                        break;
                    default:
                        return BadRequest ();
                }
                if (!string.IsNullOrEmpty (search) && search != "_") {
                    search = search.ToLower ();
                    iQueryableMails = iQueryableMails.Where (a => a.fromName.ToLower ().StartsWith (search) || a.theme.ToLower ().IndexOf (search) >= 0);
                }
                fullMails = iQueryableMails.ToArray ();
                //Подгрузка необходимых зависимостей
                var mailsCount = fullMails.Length;
                for (int i = 0; i < mailsCount; i++) {
                    var items = fullMails[i].items;
                    var itemsCount = items.Count;
                    for (int j = 0; j < itemsCount; j++) {
                        db.Entry (items[j]).Reference (a => a.item).Load ();
                        db.Entry (items[j].item).Reference (a => a.rarity).Load ();
                    }
                }

                return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (new {
                    mails = fullMails
                        .OrderByDescending (a => a.date)
                        .Skip ((pageNumber - 1) * pageSize).Take (pageSize).ToArray (),
                        pageCount = (int) Math.Ceiling ((double) fullMails.Count () / pageSize),
                        pageNumber
                }));
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Отправка сообщения
        /// </summary>
        /// <param name="message">модель сообщения</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<IActionResult> SendMail ([FromBody] ExternalMail message) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    if (message.toId == new Guid () ||
                        message.theme == "" ||
                        message.message == "")
                        return BadRequest ("Некорректный адресат, пустая тема или текст сообщения");
                    message.id = Guid.NewGuid ();
                    message.date = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                    await db.Mails.AddAsync (message);

                    var items = message.items;
                    var itemsCount = items.Length;
                    for (int i = 0; i < itemsCount; i++) {
                        var item = await db.Items.FirstOrDefaultAsync (a => a.id == items[i].item_id);
                        if (item == null)
                            return NotFound ("Не найден предмет");
                        if (item.isDeleted)
                            return BadRequest ("Предмет удален из базы");
                        var mailItem = await db.MailItems.AddAsync (new MailItems () {
                            item_id = item.id,
                                mail_id = message.id,
                                count = items[i].count
                        });
                    }

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Сбор прикрепленных к сообщению предметов
        /// </summary>
        /// <param name="userId">ИД пользователя, в чей инвентарь они будут перемещены</param>
        /// <param name="message">модель сообщения</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("{userId}")]
        public async Task<IActionResult> CollectAttachment (Guid userId, [FromBody] ExternalMail message) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var mail = await db.Mails.FindAsync (message.id);
                    if (mail == null)
                        return NotFound ("Сообщение с таким ИД не найдено");

                    var items = message.items;
                    var itemsCount = items.Length;
                    for (int i = 0; i < itemsCount; i++) {
                        var item = await db.Items.FirstOrDefaultAsync (a => a.id == items[i].item_id);
                        if (item == null)
                            return NotFound ();
                    }

                    mail.attachmentCollected = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Установка приложению сообщения признака, что оно не собрано
        /// </summary>
        /// <param name="userId">ИД юзера</param>
        /// <param name="mailId">ИД сообщения</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("userId={userId}&mailId={mailId}")]
        public async Task<IActionResult> SetAttachmentNotCollected (Guid userId, Guid mailId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var mail = await db.Mails.FindAsync (mailId);
                    if (mail == null)
                        return NotFound ("Сообщение с таким ИД не найдено");
                    mail.attachmentCollected = false;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Удаление сообщения
        /// </summary>
        /// <param name="userId">ИД юзера</param>
        /// <param name="mailId">ИД сообщения</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("userId={userId}&mailId={mailId}")]
        public async Task<IActionResult> DeleteMail (Guid userId, Guid mailId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var mail = await db.Mails.FindAsync (mailId);
                    if (mail == null)
                        return NotFound ("Сообщение с таким ИД не найдено");
                    mail.isDeleted = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Удаление нескольких сообщений
        /// </summary>
        /// <param name="userId">ИД юзера</param>
        /// <param name="mailIds">массив ИД сообщений</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("{userId}")]
        public async Task<IActionResult> DeleteMails (Guid userId, [FromBody] Guid[] mailIds) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var mailIdsCount = mailIds.Length;
                    for (int i = 0; i < mailIdsCount; i++) {
                        var mail = await db.Mails.FindAsync (mailIds[i]);
                        if (mail == null)
                            return NotFound ("Сообщение с ИД: " + mailIds[i].ToString() + " не найдено");
                        mail.isDeleted = true;
                    }
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Отметка сообщения, что оно прочитано
        /// </summary>
        /// <param name="userId">ИД юзера</param>
        /// <param name="mailId">ИД сообщения</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("userId={userId}&mailId={mailId}")]
        public async Task<IActionResult> CheckMail (Guid userId, Guid mailId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var mail = await db.Mails.FindAsync (mailId);
                    if (mail == null)
                        return NotFound ("Сообщение с таким ИД не найдено");
                    mail.isReaded = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Отправка сообщений
        /// </summary>
        /// <param name="message">Модель сообщения с несколькими адресатами</param>
        /// <returns></returns>
        [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<IActionResult> SendMails ([FromBody] MultipleMail message) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    if (message.theme == "" ||
                        message.message == "")
                        return BadRequest ("Тема или текст сообщения пусты");

                    for (int i = 0; i < message.userIds.Length; i++) {
                        var mail = new Mail ();
                        mail.id = Guid.NewGuid ();
                        mail.toId = new Guid (message.userIds[i]);
                        mail.date = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                        mail.theme = message.theme;
                        mail.message = message.message;
                        mail.fromId = message.fromId;
                        mail.fromName = message.fromName;
                        await db.Mails.AddAsync (mail);
                        var itemsCount = message.items.Count;
                        for (int j = 0; j < itemsCount; j++) {
                            var item = await db.Items.FirstOrDefaultAsync (a => a.id == message.items[j].item_id);
                            if (item == null)
                                return NotFound ("Предмет не найден");
                            if (item.isDeleted)
                                return BadRequest ("Предмет удален из базы");
                            var mailItem = new MailItems () {
                                item_id = item.id,
                                mail_id = mail.id,
                                count = message.items[j].count
                            };
                            await db.MailItems.AddAsync (mailItem);
                        }
                    }
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Отправка сообщения с вложением
        /// </summary>
        /// <param name="message">Модель сообщения с вложением</param>
        /// <returns></returns>
        [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<IActionResult> SendMailWithAttach ([FromBody] MultipleMail message) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    if (message.toId == new Guid () ||
                        message.theme == "" ||
                        message.message == "")
                        return BadRequest ("Некорректный адресат, пустая тема или текст сообщения");

                    var mail = new Mail ();
                    mail.id = Guid.NewGuid ();
                    mail.toId = message.toId;
                    mail.toName = message.toName;
                    mail.date = DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss");
                    mail.theme = message.theme;
                    mail.message = message.message;
                    mail.fromId = message.fromId;
                    mail.fromName = message.fromName;
                    await db.Mails.AddAsync (mail);
                    var itemsCount = message.items.Count;
                    for (int j = 0; j < itemsCount; j++) {
                        var item = await db.Items.FirstOrDefaultAsync (a => a.id == message.items[j].item_id);
                        if (item == null)
                            return NotFound("Предмет с ИД: " + message.items[j].item_id.ToString() + " не найден");
                        if (item.isDeleted)
                            return BadRequest ("Предмет удален из базы");
                        var mailItem = new MailItems () {
                            item_id = item.id,
                            mail_id = mail.id,
                            count = message.items[j].count
                        };
                        await db.MailItems.AddAsync (mailItem);
                    }
                    await db.SaveChangesAsync ();

                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}