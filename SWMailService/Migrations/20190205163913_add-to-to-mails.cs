﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWMailService.Migrations
{
    public partial class addtotomails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "toId",
                table: "Mails",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "toName",
                table: "Mails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "toId",
                table: "Mails");

            migrationBuilder.DropColumn(
                name: "toName",
                table: "Mails");
        }
    }
}
