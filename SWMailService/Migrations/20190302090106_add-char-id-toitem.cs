﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SWMailService.Migrations
{
    public partial class addcharidtoitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "characterId",
                table: "Items",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "characterId",
                table: "Items");
        }
    }
}
