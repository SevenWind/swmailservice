﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SWMailService.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Mails",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    fromId = table.Column<Guid>(nullable: false),
                    fromName = table.Column<string>(nullable: true),
                    theme = table.Column<string>(nullable: true),
                    date = table.Column<string>(nullable: true),
                    message = table.Column<string>(nullable: true),
                    isReaded = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mails", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Rarity",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rarity", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    code = table.Column<string>(nullable: true),
                    image = table.Column<string>(nullable: true),
                    rarity_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.id);
                    table.ForeignKey(
                        name: "FK_Items_Rarity_rarity_id",
                        column: x => x.rarity_id,
                        principalTable: "Rarity",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MailItems",
                columns: table => new
                {
                    mail_id = table.Column<Guid>(nullable: false),
                    item_id = table.Column<Guid>(nullable: false),
                    count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailItems", x => new { x.mail_id, x.item_id });
                    table.ForeignKey(
                        name: "FK_MailItems_Items_item_id",
                        column: x => x.item_id,
                        principalTable: "Items",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MailItems_Mails_mail_id",
                        column: x => x.mail_id,
                        principalTable: "Mails",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Rarity",
                columns: new[] { "id", "code" },
                values: new object[,]
                {
                    { 1, "None" },
                    { 2, "COMMON" },
                    { 3, "UNCOMMON" },
                    { 4, "RARE" }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "id", "code", "image", "name", "rarity_id" },
                values: new object[,]
                {
                    { new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), "ScrollOfHiring", "items/scroll.png", "Свиток Найма", 1 },
                    { new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), "MagicAmulet", "items/amulet.png", "Магический амулет", 1 },
                    { new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), "Coins", "items/coin.png", "Монеты", 2 },
                    { new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"), "Gold", "items/gold.png", "Золото", 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_rarity_id",
                table: "Items",
                column: "rarity_id");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_item_id",
                table: "MailItems",
                column: "item_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MailItems");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Mails");

            migrationBuilder.DropTable(
                name: "Rarity");
        }
    }
}
