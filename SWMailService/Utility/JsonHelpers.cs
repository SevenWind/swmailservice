﻿using Newtonsoft.Json;

namespace SWMailService.Utility {
    public class JsonHelpers {
        /// <summary>
        /// Сериализация объекта в json-строку с игнорированием циклических зависимостей
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <returns>json строка</returns>
        public static string SerializeObjectWithLoopIgnore<T>(T obj) {
            return JsonConvert.SerializeObject(obj,
                    Formatting.Indented,
                    new JsonSerializerSettings {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }
                );
        }
    }
}
